cmake_minimum_required(VERSION 3.10)

project(Nazwa VERSION 1.0)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")

set(CMAKE_AUTOMOC_ON)
set(CMAKE_INCLUDE_CURRENT_DIR_ON)
find_package(Qt4 4.8.7 REQUIRED QtGui QtXml)
add_executable(myexe cos.cpp)
target_link_libraries(myexe Qt4::QtGui Qt4::QtXml)
