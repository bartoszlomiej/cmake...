#include <iostream>

#pragma once

template <typename Key, typename Info>
class Ring{
    struct node;
public:
  class Iterator;
  class const_Iterator;
  Ring() : head(NULL), counter(0) {};
  ~Ring();
  Ring(const Ring<Key, Info> &x);
  Ring<Key, Info>& operator=(const Ring<Key, Info> &x);
  Ring<Key, Info>& operator=(Ring<Key, Info> && other);
  Ring(Ring&& other);
  auto begin() const { return head; }
  auto end() const { if(!head) return head; return head->prev; }
  void clear();
  bool insert(Iterator& it, const Key& k, const Info& inf);
  void deleteNode(Iterator& it);
  auto find(const Key& k, int which);
  void print();
  bool operator==(const Ring<Key, Info> &x);
  bool operator!=(const Ring<Key, Info> &x);
  bool isEmpty() const;
  int length() const {return counter;};
//  Ring<Key, Info> operator* () const { return *this; }
private:
  node* head;
  int counter; //licznik
  void copyRing(const Ring<Key, Info>& x);
  node* getHead() const{return head;};
  Iterator nullit = nullptr;
};

template <typename Key, typename Info>
Ring<Key, Info>::~Ring(){
    clear();
    std::cout << "List removed." << std::endl;
}

template <typename Key, typename Info>
void Ring<Key, Info>::clear(){  //----------works properly
  if(!head)
    return;
  Iterator it = end();
  node* buffor;
  while(it != begin()){
    buffor = *it;
    delete buffor;
    --it;
  }
  delete head;
  head = nullptr;
  counter = 0;
}

template <typename Key, typename Info>
bool Ring<Key, Info>::insert(Iterator& it, const Key &k, const Info &inf){
  Iterator it2(it);
  if(it == nullit){  //case 1 - the list is empty.
    if(!head){
      head = new node;
      head->key = k;
      head->info = inf;
      head->next = head;
      head->prev = head;
      counter++;
      it = begin();
      return true;
    }
    else{
      return false;
    }
  }
  else if(*it == *(++it2)){  //case 2 - there is only one element in the list.
    node* newNode = new node;
    newNode->key = k;
    newNode->info = inf;
    head->next = newNode;
    head->prev = newNode;
    newNode->prev = head;
    newNode->next = head;
    counter++;
    return true;
  }
  else{  //case 3 - there are at least two elements in the list
    node* newNode = new node;
    newNode->key = k;
    newNode->info = inf;
    Iterator it3(newNode);
    (*it2)->prev = *it3;
    (*it3)->next = *(it2);
    (*it)->next = *it3;
    (*it3)->prev = (*it);
    return true;
  }
  return false;
}

template <typename Key, typename Info>
void Ring<Key, Info>::print(){
  if(!head){
    std::cout << "The list is empty" << std::endl;
  }
  else{
    node* newNode = head;
    int count = 0;
    do{
      std::cout << "Element number: " << count << std::endl;
      std::cout << "Key: " << newNode->key << std::endl;
      std::cout << "Info: " << newNode->info << std::endl;
      newNode = newNode->next;
      count++;
    }while(newNode != head);
  }
}

template <typename Key, typename Info>
void Ring<Key, Info>::copyRing(const Ring<Key, Info>& x){
  node* newNode;
  Ring<Key, Info>::Iterator it(x.begin());
  if(!x.head){
    head = NULL;
    counter = 0;
  }
  else{
    counter = 1;
    head = new node;
    head->key = it.getKey();
    head->info = it.getInfo();
    head->next = head;
    head->prev = head;
    Ring<Key, Info>::Iterator itCurrent(begin());
    it++;
    while(it != x.begin()){
      newNode = new node;
      newNode->key = it.getKey();
      newNode->info = it.getInfo();
      newNode->next = head;
      newNode->prev = *itCurrent;
      (*itCurrent)->next = newNode;
      itCurrent++;
      it++;
      counter++;
      }
  }
}

template<typename Key, typename Info>
Ring<Key, Info>::Ring(const Ring<Key, Info> &x) : head(NULL), counter(0){
  head = NULL;
  copyRing(x);
}

template<typename Key, typename Info>
Ring<Key, Info>::Ring(Ring&& other){
  head = other.head;
  counter = other.counter;
  other.head = nullptr;
  other.counter = 0;
}

template<typename Key, typename Info>
Ring<Key, Info>& Ring<Key, Info>::operator=(Ring&& other){
  if(this != &other){
    clear();
    head = other.head;
    counter = other.counter;
    other.head = nullptr;
    other.counter = 0;
  }
  return *this;
}

template<typename Key, typename Info>
Ring<Key, Info>& Ring<Key, Info>::operator=(const Ring<Key, Info> &x){
  if(this != &x)
    copyRing(x);

  return *this;
}

template<typename Key, typename Info>
bool Ring<Key, Info>::operator==(const Ring<Key, Info> &x){
  if(counter != x.counter)
    return 0;

  Ring<Key, Info>::Iterator it(x.begin());
  do{
    if(it == nullit)
      return 0;
    it++;
  }while(it != x.begin());
  return 1;
}

template<typename Key, typename Info>
bool Ring<Key, Info>::operator!=(const Ring<Key, Info> &x){
  return !(*this == x);
}

template<typename Key, typename Info>
bool Ring<Key, Info>::isEmpty() const{
  if(counter == 0)
    return 1;
  return 0;
}

template<typename Key, typename Info>
void Ring<Key, Info>::deleteNode(Iterator& it){
  if(it == nullit)
    return;

  if(head == head->next){
    delete head;
    head = NULL;
    it = nullit;
    counter--;
    return;
  }

  if(*it == head)
    head = head->next;
  
  Iterator previt(it);
  previt--;
  Iterator nextit(it);
  nextit++;

  (*nextit)->prev = *previt;
  (*previt)->next = *nextit;
  delete *it;
  it = nullit;
  counter--;
}

template<typename Key, typename Info>
auto Ring<Key, Info>::find(const Key &k, int which){
  Ring<Key, Info>::Iterator it(begin());
  int i = 0;
  do{
    if(it.getKey() == k){
      i++;
      if(i == which)
	return it;
    }
    it++;
  }while(it != begin());
  return nullit;
}

//Structure node used as an element of the ring
template <typename Key, typename Info>
struct Ring<Key, Info>::node{
  Key key;
  Info info;
  node* next;
  node* prev;
};

//Itererator implementation
template<typename Key, typename Info>
class Ring<Key, Info>::Iterator : const_Iterator{
public:
  Iterator() : current(NULL){}
  Iterator(node *ptr) : current(ptr) {}
  auto* operator*() { return current; }
  Info& getInfo() const{ return current->info; }
  Key& getKey() const{ return current->key; }
  Iterator& operator=(const Iterator &x) { current = x.current; return *this; }
  Iterator operator++() { current = current->next; return *this; }
  Iterator operator++(int) { current = current->next; return *this; }
  Iterator operator--() { current = current->prev; return *this; }
  Iterator operator--(int) { current = current->prev; return *this; }
  bool operator== (const Iterator &x) const { return (current == x.current); }
  bool operator!= (const Iterator &x) const { return (current != x.current); }
private:
  node* current;
};


template<typename Key, typename Info>
class Ring<Key, Info>::const_Iterator{
public:
  const_Iterator() : current(NULL){}
  const_Iterator(node *ptr) : current(ptr) {}
  node* operator*() { return current; }
  Info& getInfo() const{ return current->info; }
  Key& getKey() const{ return current->key; }
  Iterator& operator=(const Iterator &x) { current = x.current; return *this; }
  bool operator== (const Iterator &x) const { return (current == x.current); }
  bool operator!= (const Iterator &x) const { return (current != x.current); }  
private:
  node* current;
};
