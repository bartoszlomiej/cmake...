#include <iostream>
#include "ring.hpp"
#include "shuffle.hpp"

using namespace std;

int main(){
    Ring<int, int> list1;
    Ring<int, int>::Iterator it;
    list1.insert(it, 1, 10);
    it = list1.begin();
    list1.insert(it, 2, 4);
    list1.insert(it, 1, 123);
    it = list1.end();
    list1.insert(it, 3, 12);
    list1.insert(it, 6, 0);
    list1.insert(it, 12, 99);
    list1.print();
    list1.clear();
    it = list1.end();
    list1.insert(it, 1, 123);
    it++;
    list1.insert(it, 2, 7);
    it++;
    list1.insert(it, 81, 1);
    list1.print();
    cout << "Checking find method" << endl;
    //    it = list1.begin();
    it = list1.find(7, 2);
    it = list1.find(2, 1);
    cout << it.getInfo() << endl;
    Ring<int, int> list2;
    Ring<int, int>::Iterator it2(list2.begin());
    list2.insert(it2, 22,31);
    Ring<int, int>::const_Iterator it3(list2.begin());
    cout << it3.getKey() << endl;
    
    cout << endl <<"--------comparison----------" << endl;
    Ring<int, int> list3(list1);
    if(list2 != list3)
      cout << "list2 != list3" << endl;
    cout << "-------List3-----------" << endl;
    list3.print();
    cout <<  "---------List4--------" << endl;
    Ring<int, int> list4;
    it = list4.begin();
    list4.insert(it, 5, 13);
    list4.insert(it, 8, 71);
    list4.insert(it, 21, 33);
    list4.insert(it, 9, 81);
    list4.insert(it, 49, 40);
    list4.print();
    
    cout << "---------Shuffle-------" << endl;
    list3 = shuffle(list1, list4, 5, 3, 2);
    list3.print();
    cout << "-----------------------" << endl;
   
    cout <<  "---------List1--------" << endl;
    list1.print();
    cout <<  "---------List1--------" << endl;
    cout << "List1 after delete" << endl;
    it = list1.begin();
    list1.deleteNode(it); //delete o
    it = list1.end();
    list1.deleteNode(it);
    it = list1.begin();
    list1.deleteNode(it);
    it = list1.begin();
    list1.deleteNode(it);
    list1.print();
}
