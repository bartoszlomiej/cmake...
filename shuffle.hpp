#include "ring.hpp"

#pragma once

template<typename Key, typename Info>
Ring<Key, Info>& shuffle(const Ring<Key, Info> first, const Ring<Key, Info> second, int nbfirst, int nbsecond, int reps){
  Ring<Key, Info> *result = new Ring<Key, Info>;
  typename Ring<Key, Info>::Iterator it1(first.begin()); //typename is probably used as the types key and info are unknown yet
  typename Ring<Key, Info>::Iterator it2(second.begin());
  typename Ring<Key, Info>::Iterator it3(result->begin());
  while(reps > 0){
    if(!first.isEmpty())
      for(int i = 0; i < nbfirst; i++){
	result->insert(it3, it1.getKey(), it1.getInfo());
	it1++;
	it3++;
      }
    if(!second.isEmpty())
      for(int i = 0; i < nbsecond; i++){
	result->insert(it3, it2.getKey(), it2.getInfo());
	it2++;
	it3++;
      }
    reps--;
  }
  return *result;
}
